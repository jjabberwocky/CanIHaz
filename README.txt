Our app helps people with dietary restrictions navigate the challenge of living
with  their restrictions. Users can either search the name of a product or scan 
a barcode to find out if it contains any ingredients they want to avoid. 

Step 1: Enter your allergens or restricted foods
Step 2: Take a picture of a barcode or enter the name of your desired food
Step 3: Profit.