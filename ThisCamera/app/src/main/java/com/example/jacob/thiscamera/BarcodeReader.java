package com.example.jacob.thiscamera;

import android.util.SparseArray;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.Frame;
import android.content.Context;
import android.graphics.Bitmap;

public class BarcodeReader {

    // Factories, context, etc.
    private BarcodeDetector bdetector;
    private Context context;
    private Frame.Builder fbuilder;

    // Set up factories, context, etc.
    public BarcodeReader(Context c) {
        context = c;
        bdetector = new BarcodeDetector.Builder(context).build();
        fbuilder = new Frame.Builder();
    }

    // Input: A Bitmap image (hopefully) containing a barcode
    // Output: If the image contains at least one barcode, return one of them
    // (chosen at random). Otherwise (if the barcode detector isn't loaded, or if
    // there's no barcode), return an empty string.
    public String getBarcode(Bitmap img) {
        // Check if the barcode detector is loaded; if not, return an empty string
        if (!bdetector.isOperational()) {
            return "";
        }
        // Convert the image to a Frame
        Frame frame = fbuilder.setBitmap(img).build();
        // Extract the barcode
        SparseArray<Barcode> lst = bdetector.detect(frame);
        // If there is a barcode, pick one and return its value
        if (lst.size() > 0) {
            return lst.valueAt(0).displayValue;
        }
        // Otherwise, return an empty string
        else {
            return "";
        }
    }
}