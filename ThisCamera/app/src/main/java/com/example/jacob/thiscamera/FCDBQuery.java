package com.example.jacob.thiscamera;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.xml.parsers.*;
import org.w3c.dom.*;

/*
 * Represents a query to the (USDA) Food Composition Databases.
 */
public class FCDBQuery {
    private String searchTerm;
    private int selectedNDBNo;

    /* Create a new query using the search term searchTerm. */
    public FCDBQuery(String searchTerm) {
        String editedTerm = searchTerm.replace(' ','+');
        this.searchTerm = searchTerm;
        this.selectedNDBNo = -1;
    }

    /* Queries the search database to find the NDB number of the
     * desired food, used to obtain its ingredients list. */
    public ArrayList<NameNDB> obtainResults() throws Exception {
        // Store the names and NDB numbers in an ArrayList.
        ArrayList<NameNDB> resList = new ArrayList<>();

        // Query the database and obtain the result as an XML file.
        URL url = new URL("https://api.nal.usda.gov/ndb/search/?format=xml&sort=r&ds=Branded%20Food%20Products&q=" + 
                searchTerm + "&max=1000&offset=0&api_key=AnGFoUqIfwr6IKlBSrUXCnR8kbk5OSzDpmMaAYGT");
        InputStream input = new BufferedInputStream(url.openConnection().getInputStream());

        // Set up the XML parser.
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(input);
        Element root = doc.getDocumentElement();

        // Get the names and NDB numbers of each result and add it to the results list.
        NodeList nl = doc.getElementsByTagName("item");
        for (int i = 0; i < nl.getLength(); i++) {
            Node item = nl.item(i);
            if (item.getNodeType() == Node.ELEMENT_NODE) {
                Element ele = (Element) item;
                String name = ele.getElementsByTagName("name").item(0).getTextContent();
                int ndbNo = Integer.parseInt(ele.getElementsByTagName("ndbno").item(0).getTextContent());
                resList.add(new NameNDB(name, ndbNo));
            }
        }
        
        return resList;
    }

    public void selectNDBNo(NameNDB n) {
        selectedNDBNo = n.getNDB();
    }

    public ArrayList<String> obtainIngredients() throws Exception {
        String ingredients = "";

        // Query the database and obtain the result as an XML file.
        URL url = new URL("https://api.nal.usda.gov/ndb/V2/reports?format=xml&ndbno=" + 
                selectedNDBNo + "&api_key=AnGFoUqIfwr6IKlBSrUXCnR8kbk5OSzDpmMaAYGT");
        InputStream input = new BufferedInputStream(url.openConnection().getInputStream());

        // Set up the XML parser.
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(input);
        Element root = doc.getDocumentElement();

        // Get the ingredients list as a string.
        Node food = doc.getElementsByTagName("food").item(0);
        if (food.getNodeType() == Node.ELEMENT_NODE) {
            Element ele = (Element) food;
            ingredients = ((Element) ele.getElementsByTagName("ing").item(0)).getAttribute("desc");
        }

        // Split the ingredients list into separate strings.
        String[] ingArray = ingredients.split("(,|\\(|\\[|\\]|\\)|\\:|\\.)+", -1);
        ArrayList<String> ingList = new ArrayList<String>(Arrays.asList(ingArray));
        
        return ingList;
    }

    private ArrayList<String> executeQuery() throws Exception {
        ArrayList<String> result = new ArrayList<>();

        ArrayList<NameNDB> resList = obtainResults();
        for (NameNDB n : resList) {
            result.add("Name: " + n.getName() + "\nNDB No.: " + n.getNDB());
            selectNDBNo(n);
        }

        return obtainIngredients();
    }
}

