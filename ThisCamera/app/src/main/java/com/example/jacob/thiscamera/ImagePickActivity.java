package com.example.jacob.thiscamera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ImagePickActivity extends Activity {
    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap;
    private ImageView imageView;
    private EditText editText;
    private  EditText editText2;
    public ArrayList<String> jamiesArr = new ArrayList<String>();
    private  TextView last;
    String dataFood;
    int toggle = 0;

    IngredientChecker thecheck;
    com.example.jacob.thiscamera.BarcodeReader barRead;

    /* stringname.split(); */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pick);

        barRead = new com.example.jacob.thiscamera.BarcodeReader(this); //?
        editText = (EditText) findViewById(R.id.input);
        last = (TextView) findViewById(R.id.textView7);
        final Button button = (Button) findViewById(R.id.button);
        //---------------------------------//
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button:
                        dispatchTakePictureIntent();

//                        while(returnBitMap() == null){
//                            Log.d("WHAAA", "hi baram");
//                            try {
//                                Thread.sleep(300);
//                            } catch (Exception e){
//
//                            }
//                        }
//                        String bar = barRead.getBarcode(returnBitMap());
//                        thecheck = new IngredientChecker(jamiesArr);
//                        new async().execute(dataFood);

                        /*try {
                            FCDBQuery q = new FCDBQuery(bar);
                            ArrayList<NameNDB> resList = q.obtainResults();
                            q.selectNDBNo(resList.get(0));
                            ArrayList<String> arrFinal = q.obtainIngredients();
                            ArrayList<String> lastlist= new  ArrayList<>(thecheck.check(arrFinal)); //from aaron
                            if(lastlist.size() == 0){
                                last.setText("Yes");
                            } else {
                                last.setText("NO");
                            }
                        } catch (Exception E) {
                            //DO NOTHING
                        } */
                           // break;
    //                    case R.id.button2:
    //                        sendToJamie(editText.getText().toString());
                }
                // Perform action on click

            }
        });
    }

    public void work(View v){
        EditText k = (EditText) findViewById(R.id.foodchoice);
        dataFood = k.getText().toString();
        thecheck = new IngredientChecker(jamiesArr);

//        String bar = barRead.getBarcode(returnBitMap());

        last.setText("...");
        new async().execute(dataFood);


        Log.d("YAY", dataFood);
    }

    public void clicker(View v){
        // Perform action on click
        Log.d("TO JAMIE", editText.getText().toString());
        sendToJamie(editText.getText().toString());
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            bitmap = imageBitmap;

            String bar = barRead.getBarcode(returnBitMap());

            Log.d("BAAAR", bar);
            thecheck = new IngredientChecker(jamiesArr);

            last.setText("...");
            new async().execute(bar);

            //imageView.setImageBitmap(imageBitmap);
        }
    }

    public Bitmap returnBitMap(){
       return bitmap;
    }

    public void sendToJamie(String s){
        jamiesArr.add(s);
        Log.d("THIS", jamiesArr.get(0));
    }

    private class async extends AsyncTask<String, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(String... string){
            ArrayList<String> lastlist = null;
            try {
                FCDBQuery q = new FCDBQuery(string[0]);
                ArrayList<NameNDB> resList = q.obtainResults();
                q.selectNDBNo(resList.get(0));
                Log.d("Results", resList.toString());
                ArrayList<String> arrFinal = q.obtainIngredients();
                Log.d("ARRFIN", arrFinal.toString());
//                Log.d("WHOOOPS", arrFinal.get(0));
                lastlist= thecheck.check(arrFinal); //from aaron

//                            if(lastlist.size() == 0){
//                           } else {
//                                last.setText("NO");
//                            }
            } catch (Exception E) {
                //DO NOTHING
                E.printStackTrace();
            }
        return lastlist;
        }

        @Override
        protected void onPostExecute(ArrayList<String> lastlist) {
            thecheck = new IngredientChecker(jamiesArr);
            ArrayList<String> resultlist = thecheck.check(lastlist);
                                        if(resultlist.size() == 0){
                                                last.setText("Yes");
                                            } else {
                                            last.setText("NO");
                                        }
        }
    }
}
