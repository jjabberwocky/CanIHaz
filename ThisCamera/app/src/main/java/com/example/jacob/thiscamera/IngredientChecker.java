package com.example.jacob.thiscamera;
import java.util.HashMap;
import java.util.ArrayList;

public class IngredientChecker {
    private ArrayList<String> allergies;
    private HashMap<String,String[]> foods = new HashMap<String, String[]>();
    
    public IngredientChecker(ArrayList<String> allergies){
        this.allergies = allergies;
        
        foods.put("meat", new String[] {"gelatin","rennet","carmine",
            "animal shortening", "lard"});
        foods.put("gluten", new String[] {"wheat","barley","rye","oats"});
        //https://www.foodallergy.org/allergens
        foods.put("egg", new String[] {"albumin","lysozyme","mayonnaise",
            "meringue","ovalbumin","surimi"});
        foods.put("tree nuts", new String[] {"almond","brazil nut",
            "beechnut","butternut","cashew","chestnut",
            "chinquapin nut","filbert","hazelnut","gianduja",
            "ginko nut","hickory nut","litchi","lichee","lychee",
            "macademia nut","marzipan","nangai nut","natural nut",
            "pecan","pesto","pili nut","pine nut","pistachio",
            "praline","shea nut","walnut"});
        foods.put("milk", new String[] {"butter","casein","cheese",
            "cream","curds","custard","diacetyl","ghee","half-and-half",
            "lactalbumin","llactoferrin","lactose","lactulose",
            "tagatose","whey","yogurt"});
        foods.put("dairy", new String[] {});
        
        for (String s: allergies) {
            s = s.toLowerCase();
        }
    }
    
    public ArrayList<String> check(ArrayList<String> ing){
        ArrayList<String> flagged = new ArrayList<String>();
        String allergen;
        
        for (String i: ing){
            i = i.toLowerCase();
            for (String j: allergies){
                if (foods.containsKey(j)){
                    allergen = j;
                    if (i.contains(allergen)){
                        flagged.add(i);
                    }
                    else{
                        for (int k = 0; k<foods.get(j).length; k++){
                            allergen = foods.get(j)[k];
                            if (i.contains(allergen)){
                                flagged.add(i);
                            }
                        }
                    }
                }
                else{
                    allergen = j;
                    if (i.contains(allergen)){
                        flagged.add(i);
                    }
                    
                }
            }
        }
        //returns an ArrayList of things you can't eat. If list is empty, you can eat the thing!
        return flagged;
    }
    
    public void addAllergens(ArrayList<String> allergies) {
        this.allergies.addAll(allergies);
    }
    
    public void deleteAllergens(ArrayList<String> allergies) {
        this.allergies.removeAll(allergies);
    }
}
