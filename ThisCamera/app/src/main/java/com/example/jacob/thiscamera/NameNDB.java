package com.example.jacob.thiscamera;
public class NameNDB {
    private String name;
    private int ndbNo;

    public NameNDB(String name, int ndbNo) {
        this.name = name;
        this.ndbNo = ndbNo;
    }

    public String getName() {
        return name;
    }

    public int getNDB() {
        return ndbNo;
    }
}
